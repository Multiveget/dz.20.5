// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SSNAKEGAME_SsnakeGameGameModeBase_generated_h
#error "SsnakeGameGameModeBase.generated.h already included, missing '#pragma once' in SsnakeGameGameModeBase.h"
#endif
#define SSNAKEGAME_SsnakeGameGameModeBase_generated_h

#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_SPARSE_DATA
#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_RPC_WRAPPERS
#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASsnakeGameGameModeBase(); \
	friend struct Z_Construct_UClass_ASsnakeGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASsnakeGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SsnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASsnakeGameGameModeBase)


#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASsnakeGameGameModeBase(); \
	friend struct Z_Construct_UClass_ASsnakeGameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASsnakeGameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SsnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASsnakeGameGameModeBase)


#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASsnakeGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASsnakeGameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASsnakeGameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASsnakeGameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASsnakeGameGameModeBase(ASsnakeGameGameModeBase&&); \
	NO_API ASsnakeGameGameModeBase(const ASsnakeGameGameModeBase&); \
public:


#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASsnakeGameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASsnakeGameGameModeBase(ASsnakeGameGameModeBase&&); \
	NO_API ASsnakeGameGameModeBase(const ASsnakeGameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASsnakeGameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASsnakeGameGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASsnakeGameGameModeBase)


#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_12_PROLOG
#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_SPARSE_DATA \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_RPC_WRAPPERS \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_INCLASS \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_SPARSE_DATA \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SSNAKEGAME_API UClass* StaticClass<class ASsnakeGameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SsnakeGame_Source_SsnakeGame_SsnakeGameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
