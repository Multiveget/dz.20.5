// Copyright Epic Games, Inc. All Rights Reserved.

#include "SsnakeGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SsnakeGame, "SsnakeGame" );
