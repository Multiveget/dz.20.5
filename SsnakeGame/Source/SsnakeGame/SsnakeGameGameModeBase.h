// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SsnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SSNAKEGAME_API ASsnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
